package com.app.womenproject;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.womenproject.api.ServiceHandler;
import com.app.womenproject.api.model.ApiResponse;
import com.app.womenproject.api.model.Users;
import com.app.womenproject.fragment.MessageDialogFragment;
import com.app.womenproject.helper.Keys;
import com.app.womenproject.helper.PrefernceHelper;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import rx.functions.Action1;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MapActivity";

    //Location
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 1000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private boolean mRequestingLocationUpdates = true;
    Location mCurrentLocation;


    //Map
    private SupportMapFragment mapFragment;
    private GoogleMap mGoogleMap;
    private LatLngBounds.Builder bounds = new LatLngBounds.Builder();
    private ArrayList<LatLng> latLngs = new ArrayList<>();
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        TextView title = findViewById(R.id.title);
        Button alertButton = findViewById(R.id.sendAlert);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        username = PrefernceHelper.getString(this, Keys.USERNAME);


        title.setText("Hello " + username.toUpperCase() + ". Your help is One Click away.");

        if (!PrefernceHelper.getBooolean(this, Keys.FCM_UPDATED)) {
            ServiceHandler.getInstance()
                    .updateFcm(username, FirebaseInstanceId.getInstance().getToken())
                    .subscribe(new Observer<ApiResponse<String>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(ApiResponse<String> value) {
                            if (value.status == 200) {
                                PrefernceHelper.saveBoolean(MapActivity.this, Keys.FCM_UPDATED, true);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }

        alertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServiceHandler.getInstance()
                        .sendAlert(username)
                        .subscribe(new Observer<ApiResponse<String>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(ApiResponse<String> value) {
                                if (value.status == 200) {
                                    MessageDialogFragment messageDialogFragment = new MessageDialogFragment();
                                    messageDialogFragment.setCancelable(false);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("message", "Notification has been sent to near by people.");
                                    messageDialogFragment.setArguments(bundle);
                                    messageDialogFragment.show(getSupportFragmentManager(), "");
                                } else if (value.status == 206) {
                                    MessageDialogFragment messageDialogFragment = new MessageDialogFragment();
                                    messageDialogFragment.setCancelable(false);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("message", value.data);
                                    messageDialogFragment.setArguments(bundle);
                                    messageDialogFragment.show(getSupportFragmentManager(), "");
                                }
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });
            }
        });

    }


    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public String getLocality(LatLng latng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latng.latitude, latng.longitude, 1);
            Address obj = addresses.get(0);

            StringBuilder stringBuilder = new StringBuilder();
            if (obj.getThoroughfare() != null) {
                stringBuilder.append(obj.getThoroughfare());
            }
            if (obj.getSubLocality() != null) {
                if (stringBuilder.toString().length() > 0) {
                    stringBuilder.append(" , ");
                }
                stringBuilder.append(obj.getSubLocality());
            }
            if (obj.getLocality() != null) {
                if (stringBuilder.toString().length() > 0) {
                    stringBuilder.append(" , ");
                }
                stringBuilder.append(obj.getLocality());
            }
            return stringBuilder.toString();
        } catch (IOException e) {
            return "";
        }
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                if (mCurrentLocation != null && mRequestingLocationUpdates) {
//                    getLocationAddress(mCurrentLocation);
                    LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                    updateLocation(currentLatLng);
                    bounds.include(currentLatLng);
                    mGoogleMap.addMarker(new MarkerOptions().position(currentLatLng)
                            .title(username));

                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 60));
                    mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition cameraPosition) {
                            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 60));
                            mGoogleMap.setOnCameraChangeListener(null);
                        }
                    });

                    stopLocationUpdates();
                    Log.i("Location received", String.valueOf(mCurrentLocation.getLatitude()));
                }
            }
        };
    }

    private void updateLocation(LatLng latLng) {
        ServiceHandler.getInstance()
                .updateLocation(username, latLng.latitude, latLng.longitude, getLocality(latLng))
                .subscribe(new Observer<ApiResponse<String>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiResponse<String> value) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("", e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            Log.d(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }
        mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mRequestingLocationUpdates = false;
                    }
                });
    }

    // A place has been received; use requestCode to track the request.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(MapActivity.this, "Location permission required to run the app", Toast.LENGTH_SHORT).show();
                MapActivity.this.finish();
            } else if (resultCode == RESULT_OK) {
                startLocationUpdates();
            }
        }
    }


    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                Looper.myLooper());
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");
                        //noinspection MissingPermission
                        if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
                                Looper.myLooper());
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade "
                                        + "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(MapActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be "
                                        + "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                Toast.makeText(MapActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                mRequestingLocationUpdates = false;
                        }
                    }
                });
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        googleMap.clear();
        mRequestingLocationUpdates = true;
        RxPermissions.getInstance(this)
                .request(android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean granted) {
                        if (granted) {
                            if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            googleMap.setMyLocationEnabled(true);
                            startLocationUpdates();

                            ServiceHandler.getInstance()
                                    .getAllUsers()
                                    .subscribe(new Observer<ApiResponse<List<Users>>>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(ApiResponse<List<Users>> value) {

                                            for (int i = 0; i < value.data.size(); i++) {
                                                if (value.data.get(i).lat != null && !value.data.get(i).username.equals(username)) {
                                                    LatLng latLng = new LatLng(Double.parseDouble(value.data.get(i).lat), Double.parseDouble(value.data.get(i).lng));
                                                    bounds.include(latLng);
                                                    latLngs.add(latLng);
                                                    mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                                                            .title(value.data.get(i).username));
                                                }
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onComplete() {
                                            if (latLngs.size() > 0) {
                                                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 60));
                                                mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                                                    @Override
                                                    public void onCameraChange(CameraPosition cameraPosition) {
                                                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 60));
                                                        mGoogleMap.setOnCameraChangeListener(null);
                                                    }
                                                });
                                            }
                                        }
                                    });
                        } else {
                            Log.i(TAG, "Permission Required");
                            Toast.makeText(MapActivity.this, "Location permission required to run the app", Toast.LENGTH_SHORT).show();
                            MapActivity.this.finish();
                        }
                    }
                });
    }
}
