package com.app.womenproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.app.womenproject.helper.Keys;
import com.app.womenproject.helper.PrefernceHelper;

/**
 * Created by nitheesh on 27/3/18.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent;
        if (PrefernceHelper.getBooolean(this, Keys.USER_REGISTERED)) {
            intent = new Intent(this, MapActivity.class);
        } else {
            intent = new Intent(this, RegistrationActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
