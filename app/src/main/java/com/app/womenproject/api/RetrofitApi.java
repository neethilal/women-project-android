package com.app.womenproject.api;

import com.app.womenproject.api.model.ApiResponse;
import com.app.womenproject.api.model.Users;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nitheesh on 27/3/18.
 */

public interface RetrofitApi {

    String BASE_URL = "https://nodepsqlheroku.herokuapp.com/";

    @GET("db/createUser")
    Observable<ApiResponse<String>> createUser(@Query("username") String username);

    @GET("db/updateFcm")
    Observable<ApiResponse<String>> updateFcm(@Query("username") String username, @Query("fcm") String token);

    @GET("db/updateLocation")
    Observable<ApiResponse<String>> updateLocation(@Query("username") String username, @Query("lat") double latitude, @Query("lng") double longitude, @Query("location") String location);

    @GET("/db/sendAlert")
    Observable<ApiResponse<String>> sendAlert(@Query("username") String username);

    @GET("/db/getUsersList")
    Observable<ApiResponse<List<Users>>> getUsersList();
}
