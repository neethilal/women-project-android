package com.app.womenproject.api.model;

import java.io.Serializable;

/**
 * Created by nitheesh on 27/3/18.
 */

public class Users implements Serializable {

    public String username;
    public String fcm;
    public String lat;
    public String lng;
    public String location;
}
