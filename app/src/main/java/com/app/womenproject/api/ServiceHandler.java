package com.app.womenproject.api;


import com.app.womenproject.api.model.ApiResponse;
import com.app.womenproject.api.model.Users;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by nitheesh on 27/3/18.
 */

public class ServiceHandler {

    private static ServiceHandler serviceHandler;
    private static RetrofitApi api;

    public static ServiceHandler getInstance() {
        if (serviceHandler == null) {
            api = RetrofitService.getInstance().getApi();
            return serviceHandler = new ServiceHandler();
        }
        return serviceHandler;
    }

    public Observable<ApiResponse<String>> createUser(String username) {
        return api.createUser(username).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
    }

    public Observable<ApiResponse<String>> updateFcm(String username, String token) {
        return api.updateFcm(username, token).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
    }

    public Observable<ApiResponse<String>> updateLocation(String username, double latitude, double longitude, String location) {
        return api.updateLocation(username, latitude, longitude, location).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
    }

    public Observable<ApiResponse<String>> sendAlert(String username) {
        return api.sendAlert(username).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
    }

    public Observable<ApiResponse<List<Users>>> getAllUsers() {
        return api.getUsersList().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
    }
}
